## try on https://gitlab.com/-/graphql-explorer
## Group structure query

{
  group(fullPath: "jrreid-gold") {
    vulnerabilitySeveritiesCount {
      critical
      high
      medium
      low
      info
    }
    projectsCount
    descendantGroupsCount
    descendantGroups(includeParentDescendants: false) {
      edges {
        node {
          name
          fullPath
          projectsCount
          vulnerabilitySeveritiesCount {
            critical
            high
            medium
            low
            info
          }
          descendantGroupsCount
        }
      }
    }
  }
}
