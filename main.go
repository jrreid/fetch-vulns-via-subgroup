package main

import (
	"context"
	"os"

	"github.com/joho/godotenv"
	"github.com/shurcooL/graphql"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	src := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: os.Getenv("GITLAB_TOKEN")},
	)

	//log.Info(os.Getenv("GITLAB_TOKEN"))
	httpClient := oauth2.NewClient(context.Background(), src)
	client := graphql.NewClient("https://gitlab.com/api/graphql", httpClient)

	// Define which group path to execute the graphQL query on
	queryVariables := map[string]any{
		"fullPath": "jrreid-gold",
	}

	var queryGroups QueryGroupsType

	// Perform the graphQL query to retrieve group structure
	err = client.Query(context.Background(), &queryGroups, queryVariables)
	if err != nil {
		log.Error(err)
		return
	} else {
		log.Info("Group Query Successful")
	}

	// log results
	log.Infof("Group Queried: %s", queryVariables["fullPath"])
	log.Infof("Descendant Groups: %d", queryGroups.Group.DescendantGroupsCount)
	log.Infof("Project Count: %d", queryGroups.Group.ProjectsCount)
	logVulnCountsBySeverity(queryGroups.Group.VulnerabilitySeveritiesCount)

	for _, edge := range queryGroups.Group.DescendantGroups.Edges {
		log.Warnf("Descendant Group: %s", edge.Node.Name)
		log.Infof("Descendant Group Path: %s", edge.Node.FullPath)
		log.Infof("Descendant Group Project Count: %d", edge.Node.ProjectsCount)
		logVulnCountsBySeverity(edge.Node.VulnerabilitySeveritiesCount)
		queryVariables = map[string]any{
			"fullPath": string(edge.Node.FullPath),
		}

		var queryVulns QueryVulnsType
		err = client.Query(context.Background(), &queryVulns, queryVariables)
		if err != nil {
			log.Error(err)
			return
		} else {
			log.Info("Vulnerability Query Successful")
		}
	}
}

type VulnerabilitySeveritiesCountType struct {
	Critical graphql.Int
	High     graphql.Int
	Medium   graphql.Int
	Low      graphql.Int
	Info     graphql.Int
}

type QueryGroupsType struct {
	Group struct {
		VulnerabilitySeveritiesCount VulnerabilitySeveritiesCountType

		ProjectsCount         graphql.Int
		DescendantGroupsCount graphql.Int

		DescendantGroups struct {
			Edges []struct {
				Node struct {
					Name                         graphql.String
					FullPath                     graphql.String
					ProjectsCount                graphql.Int
					VulnerabilitySeveritiesCount VulnerabilitySeveritiesCountType
					DescendantGroupsCount        graphql.Int
				}
			}
		} `graphql:"descendantGroups(includeParentDescendants: false)"`
	} `graphql:"group(fullPath: $fullPath)"`
}

type QueryVulnsType struct {
	Group struct {
		Vulnerabilities struct {
			PageInfo struct {
				EndCursor   graphql.String
				StartCursor graphql.String
				HasNextPage graphql.Boolean
			}
			Edges []struct {
				Node struct {
					Project struct {
						Id       graphql.String
						FullPath graphql.String
						WebUrl   graphql.String
					}
					Id          graphql.String
					Description graphql.String
					Identifiers []struct {
						ExternalType graphql.String
						ExternalId   graphql.String
						Name         graphql.String
					}
					Links []struct {
						Url graphql.String
					}
					Severity graphql.String
					State    graphql.String
					WebUrl   graphql.String
				}
			}
		}
	} `graphql:"group(fullPath: $fullPath)"`
}

func logVulnCountsBySeverity(VulnerabilitySeveritiesCount VulnerabilitySeveritiesCountType) {
	log.Infof("Count of group's vulnerabilities by severity:\n  Critical: %d\n  High: %d\n  Medium: %d\n  Low: %d\n  Info: %d\n",
		VulnerabilitySeveritiesCount.Critical,
		VulnerabilitySeveritiesCount.High,
		VulnerabilitySeveritiesCount.Medium,
		VulnerabilitySeveritiesCount.Low,
		VulnerabilitySeveritiesCount.Info)
}
