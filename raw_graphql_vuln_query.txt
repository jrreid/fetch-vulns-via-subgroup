## try on https://gitlab.com/-/graphql-explorer
## Vulnerabilities by group query
{
  group(fullPath: "jrreid-gold") {
    vulnerabilities {
      pageInfo {
        endCursor
        startCursor
        hasNextPage
      }
      edges {
        node {
          project {
            id
            fullPath
            webUrl
          }
          id
          description
          identifiers {
            externalType
            externalId
            name
          }
          links {
            url
          }
          severity
          state
          webUrl
          resolvedOnDefaultBranch
        }
      }
    }
  }
}
