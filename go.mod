module fetch-vulns-via-subgroup

go 1.21.7

require (
	github.com/shurcooL/graphql v0.0.0-20230722043721-ed46e5a46466
	golang.org/x/oauth2 v0.17.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
